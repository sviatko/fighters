import { arenaService } from './arenaService';
import { getDamage } from '../components/fight';
import { fighterService } from './fightersService';
import Fighter from '../models/Fighter';
import { HealthIndicatorPosition } from '../types/ui';

class FightService {
  hit(
    fighter: Fighter,
    defenderOrigin: Fighter,
    defenderCopy: Fighter,
    defenderPosition: HealthIndicatorPosition
  ) {
    const damage = getDamage(fighter, defenderCopy);
    defenderCopy.health -= damage;

    const healthIndicator = fighterService.calculateRemainingHealthInPercents(
      defenderOrigin.health,
      defenderCopy.health
    );
    arenaService.reduceHealthIndicator(healthIndicator, defenderPosition);
  }

  criticalHit(
    fighter: Fighter,
    defenderOrigin: Fighter,
    defenderCopy: Fighter,
    defenderPosition: HealthIndicatorPosition
  ) {
    const criticalHitIndicator = defenderPosition === 'left' ? 'right' : 'left';

    defenderCopy.health -= fighter.attack * 2;

    const healthIndicator = fighterService.calculateRemainingHealthInPercents(
      defenderOrigin.health,
      defenderCopy.health
    );

    arenaService.reduceHealthIndicator(healthIndicator, defenderPosition);
    arenaService.increaseCriticalHitIndicator(10, criticalHitIndicator);
  }
}

export const fightService = new FightService();
