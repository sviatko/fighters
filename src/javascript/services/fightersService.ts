import { callApi } from '../helpers/apiHelper';
import Fighter from '../models/Fighter';

class FighterService {
  async getFighters(): Promise<Fighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi<Fighter[]>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails<T>(id: string): Promise<T> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi<T>(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  calculateRemainingHealthInPercents(
    originalHealth: number,
    currentHealth: number
  ): number {
    return (currentHealth / originalHealth) * 100;
  }
}

export const fighterService = new FighterService();
