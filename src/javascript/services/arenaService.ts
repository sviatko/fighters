import { HealthIndicatorPosition } from '../types/ui';

class ArenaService {
  reduceHealthIndicator(healthLeft: number, position: HealthIndicatorPosition) {
    const healthElement = document.getElementById(`${position}-fighter-indicator`);

    if (healthElement) healthElement.style.width = `${healthLeft}%`;
  }

  increaseCriticalHitIndicator(time: number, position: HealthIndicatorPosition) {
    const indicator = document.getElementById(`${position}-fighter-critical-hit-indicator`);

    indicator.style.width = `${(10 - time) * 10}%`;
  }
}

export const arenaService = new ArenaService();
