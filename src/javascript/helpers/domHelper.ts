import { IDOMElement } from '../interfaces/IDOMElement';
import { CreateElementAttributes } from '../types/ui';

export function createElement({
  tagName,
  className,
  attributes = {},
}: IDOMElement<string>) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key: CreateElementAttributes) =>
    element.setAttribute(key, attributes[key])
  );

  return element;
}
