import { UserCanClick } from '../types/fighter';

/**
 * Returns random number between 1 and 2
 *
 * @param {number} max
 * @param {number} min
 *
 * @returns {number}
 */
export function getRandom(max: number = 2, min: number = 1): number {
  min = Math.ceil(min);
  max = Math.floor(max);

  return Math.random() * (max - min + 1) + min;
}

/**
 * Returns TRUE if the first specified array contains all elements
 * from the second one. FALSE otherwise.
 *
 * @param {array} superset
 * @param {array} subset
 *
 * @returns {boolean}
 */
export function arrayContainsArray(
  superset: UserCanClick[],
  subset: UserCanClick[]
): boolean {
  if (0 === subset.length) {
    return false;
  }
  return subset.every(function (value) {
    return superset.indexOf(value) >= 0;
  });
}
