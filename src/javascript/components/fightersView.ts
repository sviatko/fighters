import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import Fighter from '../models/Fighter';
import { СreateFightersSelectorFunc } from '../interfaces/СreateFightersSelectorFunc';

export function createFighters(fighters: Fighter[]) {
  const selectFighter = createFightersSelector();
  const container = createElement({
    tagName: 'div',
    className: 'fighters___root',
  });
  const preview = createElement({
    tagName: 'div',
    className: 'preview-container___root',
  });
  const fightersList = createElement({
    tagName: 'div',
    className: 'fighters___list',
  });
  const fighterElements = fighters.map((fighter) =>
    createFighter(fighter, selectFighter)
  );

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(
  fighter: Fighter,
  selectFighter: СreateFightersSelectorFunc
) {
  const fighterElement = createElement({
    tagName: 'div',
    className: 'fighters___fighter',
  });
  const imageElement = createImage(fighter);
  const onClick = (event: Event) => selectFighter(fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: Fighter) {
  const { source, name } = fighter;
  const attributes = { src: source, alt: name };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes,
  });

  return imgElement;
}
