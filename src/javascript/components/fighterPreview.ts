import { createElement } from '../helpers/domHelper';
import Fighter from '../models/Fighter';
import { HealthIndicatorPosition } from '../types/ui';

export function createFighterPreview(
  fighter: Fighter,
  position: HealthIndicatorPosition
) {
  const positionClassName =
    position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const nameElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___name',
    });
    nameElement.innerText = fighter.name;

    const imageElement = createElement({
      tagName: 'img',
      className: 'fighter-preview___image',
      attributes: { src: fighter.source },
    });

    const infoWrapperElement = createElement({
      tagName: 'div',
      className: 'fighter-preview___adventures-block',
    });

    infoWrapperElement.innerText = `health: ${fighter.health}
    attack: ${fighter.attack} 
    defense: ${fighter.defense}`;

    fighterElement.append(nameElement);
    fighterElement.append(imageElement);
    fighterElement.append(infoWrapperElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter: Fighter) {
  const { source, name } = fighter;
  const attributes = { src: source, alt: name };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
