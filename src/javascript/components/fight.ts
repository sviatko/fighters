import { controls } from '../../constants/controls';
import { getRandom, arrayContainsArray } from '../helpers/commonHelper';
import { arenaService } from '../services/arenaService';
import { fightService } from '../services/fightService';

import Fighter from '../models/Fighter';
import { UserFight } from '../types/fighter';

export async function fight(
  firstFighter: Fighter,
  secondFighter: Fighter
): Promise<Fighter> {
  const firstFighterCopy = { ...firstFighter };
  const secondFighterCopy = { ...secondFighter };

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let firstFighterTime = 0;
    let secondFighterTime = 0;

    // countdown for fighter's critical hit time
    // when 0, fighter is allowed for critical hit
    const interval = setInterval(() => {
      if (firstFighterTime > 0) {
        firstFighterTime--;

        arenaService.increaseCriticalHitIndicator(firstFighterTime, 'left');
      }

      if (secondFighterTime > 0) {
        secondFighterTime--;

        arenaService.increaseCriticalHitIndicator(secondFighterTime, 'right');
      }
    }, 1000);

    let map: UserFight = {};

    document.addEventListener(
      'keydown',
      function _listener(event) {
        map[event.code] = true;

        if (!event.repeat) {
          switch (true) {
            case arrayContainsArray(
              Object.keys(map),
              controls.PlayerOneCriticalHitCombination
            ) && firstFighterTime === 0:
              fightService.criticalHit(
                firstFighter,
                secondFighter,
                secondFighterCopy,
                'right'
              );
              firstFighterTime = 10;
              break;
            case arrayContainsArray(
              Object.keys(map),
              controls.PlayerTwoCriticalHitCombination
            ) && secondFighterTime === 0:
              fightService.criticalHit(
                secondFighter,
                firstFighter,
                firstFighterCopy,
                'left'
              );
              secondFighterTime = 10;
              break;
            case map.hasOwnProperty(controls.PlayerOneAttack) &&
              !map.hasOwnProperty(controls.PlayerOneBlock) &&
              !map.hasOwnProperty(controls.PlayerTwoBlock) &&
              map[controls.PlayerOneAttack]:
              fightService.hit(
                firstFighter,
                secondFighter,
                secondFighterCopy,
                'right'
              );
              break;
            case map.hasOwnProperty(controls.PlayerTwoAttack) &&
              !map.hasOwnProperty(controls.PlayerOneBlock) &&
              !map.hasOwnProperty(controls.PlayerTwoBlock) &&
              map[controls.PlayerTwoAttack]:
              fightService.hit(
                secondFighter,
                firstFighter,
                firstFighterCopy,
                'left'
              );
              break;
          }

          if (firstFighterCopy.health <= 0 || secondFighterCopy.health <= 0) {
            clearInterval(interval);
            document.removeEventListener('keydown', _listener, true);

            if (firstFighterCopy.health <= 0) {
              arenaService.reduceHealthIndicator(0, 'left');
              resolve(secondFighter);
            }

            if (secondFighterCopy.health <= 0) {
              arenaService.reduceHealthIndicator(0, 'right');
              resolve(firstFighterCopy);
            }
          }
        }
      },
      true
    );

    document.addEventListener('keyup', (event) => {
      delete map[event.code];
    });
  });
}

export function getDamage(attacker: Fighter, defender: Fighter): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter: Fighter): number {
  const criticalHitChance = getRandom();
  const attack = fighter.attack;

  return attack * criticalHitChance;
}

export function getBlockPower(fighter: Fighter): number {
  const dodgeChance = getRandom();
  const defense = fighter.defense;

  return defense * dodgeChance;
}
