import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';
import Fighter from '../../models/Fighter';
import { IModal } from '../../interfaces/IModal';

export function showWinnerModal(fighter: Fighter): void {
  const bodyElement = createElement({
    tagName: 'div',
    className: 'modal___winner-wrapper',
  });
  const imgElement = createElement({
    tagName: 'img',
    className: 'modal___winner-img',
    attributes: { src: fighter.source },
  });
  bodyElement.append(imgElement);

  const attributes: IModal = {
    title: `${fighter.name} won!`,
    bodyElement: bodyElement,
    onClose: homepage,
  };

  showModal(attributes);
}

async function homepage() {
  App.rootElement.innerHTML = '';

  new App();
}
