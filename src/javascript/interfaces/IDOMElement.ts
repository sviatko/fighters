import { CreateElementTags, CreateElementAttributes } from '../types/ui';

export interface IDOMElement<T> {
  tagName: CreateElementTags;
  className?: string;
  attributes?: {
    [key in CreateElementAttributes]?: T;
  };
}
