export interface IModal {
  title: string;
  bodyElement: HTMLElement;
  onClose(): void;
}
