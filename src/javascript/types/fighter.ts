type PlayerOneCriticalHitCombinationSet = 'KeyQ' | 'KeyW' | 'KeyE';
type PlayerTwoCriticalHitCombinationSet = 'KeyU' | 'KeyI' | 'KeyO';
type PlayerOneHit = 'KeyA';
type PlayerOneBlock = 'KeyD';
type PlayerTwiHit = 'KeyJ';
type PlayerTwiBlock = 'KeyL';

export type UserCanClick =
  | PlayerOneCriticalHitCombinationSet
  | PlayerTwoCriticalHitCombinationSet
  | PlayerOneHit
  | PlayerOneBlock
  | PlayerTwiHit
  | PlayerTwiBlock
  | string;

export type UserFight = { [K in UserCanClick]: boolean };
