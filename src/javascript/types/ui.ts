export type HealthIndicatorPosition = 'left' | 'right';
export type CreateElementTags = 'div' | 'img' | 'span' | 'button';
export type CreateElementAttributes = 'id' | 'src' | 'alt';
