export default class FighterPreview {
  readonly _id: string;
  name: string;
  source: string;
}
